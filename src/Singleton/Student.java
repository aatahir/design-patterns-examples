package Singleton;

public class Student {
	private String name, address;
	private int id;

	public Student(String name, String address, int id) {
		super();
		this.name = name;
		this.address = address;
		this.id = id;
	}

	private Student() {
	}

	// singleton object - private static which cannot be accessed from outside the class.
	private static Student instance = new Student();

	// get an instance of the object
	public static Student getInstance() {
		return instance;
	}

	// just to check that the object has been successfully created.
	public void check() {
		System.out.println("object successfully created!!");
	}
}
