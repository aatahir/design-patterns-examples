package factory;

public class Square implements Shape {

	private double length;

	public Square(double length) {
		this.length = length;
	}

	@Override
	public double draw() {
		return length * length;
	}

}
