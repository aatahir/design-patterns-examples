package factory;

import java.lang.Math.*;

public class Circle implements Shape {
	private double radius = 0;

	public Circle(double radius) {
		super();
		this.radius = radius;
	}

	@Override
	public double draw() {
		return Math.PI * (Math.pow(radius, 2));
	}

}
