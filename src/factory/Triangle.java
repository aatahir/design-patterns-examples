package factory;

public class Triangle implements Shape {
	private double base, height;

	public Triangle(double base, double hight) {
		super();
		this.base = base;
		this.height = hight;
	}

	@Override
	public double draw() {
		return (base * height) / 2;
	}

}
