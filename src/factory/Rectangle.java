package factory;

public class Rectangle implements Shape {

	private double width, length;

	public Rectangle(double width, double length) {
		super();
		this.width = width;
		this.length = length;
	}

	@Override
	public double draw() {
		return width * length;
	}

}
