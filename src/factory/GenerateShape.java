package factory;

public class GenerateShape {

	public static void main(String[] args) {

		ShapeFactory sf = new ShapeFactory();

		// get an object of Circle and call its draw method.
		Shape circle = sf.getShape("C");
		System.out.println("circle: " + circle.draw());

		// get an object of Rectangle and call its draw method.
		Shape rectangle = sf.getShape("R");
		System.out.println("rectangle: " + rectangle.draw());

		// get an object of Square and call its draw method.
		Shape square = sf.getShape("S");
		System.out.println("square :" + square.draw());

		// get an object of Triangle and call its draw method.
		Shape triangle = sf.getShape("T");
		System.out.println("triangle :" + triangle.draw());
	}

}
