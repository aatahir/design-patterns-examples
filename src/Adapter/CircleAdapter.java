package Adapter;

public class CircleAdapter implements Shape {
	 
	private Circle adaptee;

	    public CircleAdapter(Circle circle) {
	        this.adaptee = circle;
	    }

		@Override
		public void draw() {
			adaptee.draw();
		}
}
