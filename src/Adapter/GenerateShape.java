package Adapter;

public class GenerateShape {
	
	private static int width =10;
	private static int height = 20;
	private static int base = 5;
	private static int radius = 5;
	
	public static void main(String[] args) {
		
		Shape rectangleDraw = new RectangleAdapter(new Rectangle(width,height));
		rectangleDraw.draw();
		
		Shape triangleDraw = new TriangleAdapter(new Triangle(base,height));
		triangleDraw.draw();
		
		Shape squareDraw = new SquareAdapter(new Square(height));
		squareDraw.draw();
		
		Shape circleDraw = new CircleAdapter(new Circle(radius));
		circleDraw.draw();	
	}
}
