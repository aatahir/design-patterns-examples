package Adapter;

public class RectangleAdapter implements Shape {

    private Rectangle adaptee;


    public RectangleAdapter(Rectangle rectangle) {
        this.adaptee = rectangle;
        
    }

	@Override
	public void draw() {
		adaptee.draw();
	}

}
