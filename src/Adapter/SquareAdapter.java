package Adapter;

public class SquareAdapter implements Shape {

    private Square adaptee;

    public SquareAdapter(Square square) {
        this.adaptee = square;
    }

	@Override
	public void draw() {
		adaptee.draw();
	}

}
