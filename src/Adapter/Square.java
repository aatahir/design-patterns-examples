package Adapter;

public class Square  {
	
	private int height;
	
    public Square( int height) {
		this.height = height;
	}

	public void draw() {
    	System.out.println("Drawing a square, , size of " +  height*height+ "cm");
	}
}
