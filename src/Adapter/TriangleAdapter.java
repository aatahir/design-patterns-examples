package Adapter;

public class TriangleAdapter implements Shape{
	private Triangle adaptee;
	
    public TriangleAdapter(Triangle triangle) {
        this.adaptee = triangle;
    }

	@Override
	public void draw() {
		adaptee.draw();
	}
}
