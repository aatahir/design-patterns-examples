package Adapter;

public class Rectangle {

	private int width, height;
	
    public Rectangle(int width, int height) {
		super();
		this.width = width;
		this.height = height;
	}


	public void draw() {
    	System.out.println("Drawing a rectangle, size of " +  width*height+ "cm");
	}

}
