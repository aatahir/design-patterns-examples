package Adapter;

import java.lang.Math.*;

public class Circle {

	private int radius;
	
	public Circle(int radius) {
		this.radius = radius;
	}

	public void draw() {
		System.out.println("Drawing a cricle, size of " +Math.PI * (Math.pow(radius, 2))+ "cm");
	}

}
