package Adapter;

public class Triangle{
	private int base, height;
	
	
	public Triangle(int base, int height) {
		super();
		this.base = base;
		this.height = height;
	}


	public void draw() {
		System.out.println("Drawing a triangle, size is " + (base * height) / 2 +"cm");
	}

}
